const fs = require( "fs" );
const path = require( "path" );
const { exec } = require( "child_process" );
const liveServer = require( "live-server" );

const packageRoot = path.dirname(__dirname);

const testRequirements = [
	`${packageRoot}/node_modules/jquery/dist/jquery.min.js`,
	`${packageRoot}/dist/jpiechart.js`
];

function preTestSetup() {
	console.log("Source files modified.");
	exec("rollup -c --environment development", error => { 
		if(error) {
			console.log( error );
			process.exit(1);
		}	
	});

	for( var i = 0; i < testRequirements.length; i++ ) {
		var streamIn = fs.createReadStream( testRequirements[i] );
		streamIn.on("error", (error) => {
			console.log( error );
			process.exit(1); 
		});

		var streamOut = fs.createWriteStream( `${packageRoot}/test/` + path.basename(testRequirements[i]) );
		streamOut.on("error", (error) => {
			console.log( error );
			process.exit(1);
		});

		streamIn.pipe(streamOut);
	}
}

fs.watch( `${packageRoot}/src/js/`, {}, preTestSetup );

liveServer.start({
	root: `${packageRoot}/test/`,
	file: "index.html",
	ignore: "jquery.min.js"
});
