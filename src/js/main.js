/**
 * See (http://jquery.com/).
 * @name jQuery
 * @class
 * See the jQuery Library  (http://jquery.com/) for full details.  This just
 * documents the function and classes that are added to jQuery by this plug-in.
 */

/**
 * See (http://jquery.com/)
 * @name fn
 * @class
 * See the jQuery Library  (http://jquery.com/) for full details.  This just
 * documents the function and classes that are added to jQuery by this plug-in.
 * @memberOf jQuery
 */

import $ from "jquery";
import {
	select as d3_select,
	selectAll as d3_selectAll,
	format as d3_format,
	pie as d3_pie,
	arc as d3_arc
} from "d3";
import * as d3plus from "d3plus-text";

/**
 * PieChart - Create a new pie chart in targetted container.
 * @memberof jQuery.fn
 */
$.fn.PieChart = function( config ) {

	// Initialiser
	new PieChart( this[ 0 ], config );

	return this;
};




/**
 * Declare prototype object for chord diagrams.
 * @private
 */
export function PieChart( element, config ) {

	//
	// Private variables.
	//

	var instanceId = document.getElementsByClassName( "common-pie-chart" ).length;

	var svg = null;

	var innerRadius = 0;

	var padAngle = 0.015, // effectively dictates the gap between slices
		floatFormat = d3_format( ".4r" ),
		cornerRadius = 3; // sets how rounded the corners are on each slice



	// Private methods.

	var color = function( i ) {
		if ( !config.colours || !config.colours.length ) return "#000000";
		return (config.colours[i % config.colours.length]);
	};
	var mouseOverHandler = function( d, i, n ) {
		for ( var j = 0; j < n.length; j++ ) {
			if ( j !== i ) {
				d3_select( n[ j ] ).attr( "opacity", 0.4 );
			}
		}
		if ( config.tooltip ) {

			svg.select( ".tooltipBackground" )
				.attr( "fill", color( d.data.colourIndex !== undefined ? d.data.colourIndex : i ) )
				.attr( "opacity", 0.4 );
			var tooltipData = d.data.tooltipData;
			var string = "";
			for ( var i = 0; i < tooltipData.length; i++ ) {
				string += "" + tooltipData[ i ].key + ": " + tooltipData[ i ].value + "\n";
			}
			svg.select("g#tooltipText-" + instanceId).selectAll("*").remove();
			var sideLength = 2 * Math.sqrt( Math.pow( innerRadius, 2 ) / 2 );
			var tooltip = new d3plus.TextBox()
				.data( [ { text: string } ] )
				.fontMin( 4 )
				.fontSize( 4 )
				.width( sideLength )
				.height( sideLength )
				.x( -sideLength / 2 )
				.y( -sideLength / 2 )
				.textAnchor( "middle" )
				.verticalAlign( "middle" )
				.select( "#tooltipText-" + instanceId )
				.render();
			svg.select( "#tooltipText- + instanceId" ).attr( "opacity", 1.0 );

		}

	};

	var mouseOutHandler = function( d, i, n ) {
		for ( var j = 0; j < n.length; j++ ) {
			d3_select( n[ j ] ).attr( "opacity", 1.0 );
		}
		if ( config.tooltip ) {

			svg.select( ".tooltipBackground" )
				.attr( "opacity", 0.0 );
			svg.select( ".d3plus-textBox" ).attr( "opacity", 0.0 );

		}
	};

	var mouseMoveHandler = function() {
		if ( config.tooltip ) {

		}
	};

	var clickHandlerWrapper = function( d ) {
		if ( config.onClick != null ) {
			config.onClick( d.data.label );
		}
	};

	var draw = function() {

		d3_select( element ).datum( config.data );

		var radius = 35;

		// creates a new pie generator
		var pie = d3_pie()
			.value( function( d ) {
				return floatFormat( d.value );
			} )
			.sort( null );

		// contructs and arc generator. This will be used for the donut. The difference between outer and inner
		// radius will dictate the thickness of the donut
		var arc = d3_arc()
			.outerRadius( radius * 1.0 )
			.innerRadius( radius * 0.9 )
			.cornerRadius( cornerRadius )
			.padAngle( padAngle );
		innerRadius = arc.innerRadius()();

		var textArc = d3_arc()
         .outerRadius( radius * 0.65 )
         .innerRadius( radius * 0.65 );

		// append the svg object to the selection
		svg = d3_select( element ).append( "svg" )
			.attr( "class", "common-pie-chart" )
			.attr( "viewBox", "0 0 100 100" )
			.attr("preserveAspectRatio", "xMidYMin meet")
			.append( "g" )
			.attr( "transform", "translate(" + 50  + "," + 50 + ")" );

		// g elements to keep elements within svg modular
		svg.append( "g" ).attr( "class", "lines" );
		svg.append( "g" ).attr( "class", "slices" );
		svg.append( "g" ).attr( "class", "labelName" );

		// add and colour the donut slices
		svg.select( ".slices" )
			.datum( config.data ).selectAll( "path" )
			.data( pie )
			.enter().append( "path" )
			.attr( "fill", function( d, i ) {
				return color( d.data.colourIndex !== undefined ? d.data.colourIndex : i );
			} )
			.attr( "d", arc )
			.attr( "data-label", function( d ) {
				return d.label 
			} )
			.on( "mouseover", mouseOverHandler )
			.on( "mouseout", mouseOutHandler )
			.on( "mousemove", mouseMoveHandler )
			.on( "click", clickHandlerWrapper )
			.style( "cursor", "pointer" )
			.attr( "id", function( d, i ){
				return "tab-" + i;
			} );
		;

		if ( config.tooltip ) {

			svg.append( "circle" )
				.attr( "class", "tooltipBackground" )
				.attr( "r", 0.95 * arc.innerRadius()() )
				.attr( "opacity", 0.0 );
			svg.append( "g" )
				.attr( "id", "tooltipText-" + instanceId );
		}

	};


	draw();

}
