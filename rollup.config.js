import { terser } from "rollup-plugin-terser";
import resolve from "rollup-plugin-node-resolve";
import commonjs from "rollup-plugin-commonjs";

const production = {
	input: "src/js/main.js",
	output: {
		name: "common.util.piechart",
		file: "dist/jpiechart.min.js",
		format: "iife",
		globals: {
			"jquery": "$"
		},
		sourceMap: "inline"
	},
	external: [ "jquery" ],
	plugins: [
		resolve(
			{
				jsnext: true,
				main: true,
				browser: true
			}
		),
		commonjs( {
			include: [ "node_modules/**" ]
		} ),
		terser()
	]
};

const development = {
	input: "src/js/main.js",
	output: {
		name: "common.util.piechart",
		file: "dist/jpiechart.js",
		format: "iife",
		globals: {
			"jquery": "$"
		},
		sourceMap: "inline"
	},
	external: [ "jquery" ],
	plugins: [
		resolve(
			{
				jsnext: true,
				main: true,
				browser: true
			}
		),
		commonjs( {
			include: [ "node_modules/**" ]
		} )
	]
};

const all = [
	production,
	development
];


var exportedBuild;

if(process.env.production) {
	exportedBuild = production;
} else if (process.env.development) {
	exportedBuild = development;
} else {
	exportedBuild = all;
}

export default exportedBuild;
