# jPieChart

### Usage:

```javascript
$("PIE CHART CONTAINER").PieChart( {
   tooltip: true,
   data: [
      {
         label: "Redcorp",
         value: 37000,
         tooltipData: [             
            { key: "Statistic 1", value: "Yes" },
            { key: "Statistic 2", value: "£90,000" }, 
            { key: "Statistic 3", value: "0.05%" }
         ]
      },
      {
         label: "Blue Enterprises", 
         value: 51445,
         tooltipData: [
            { key: "Statistic 1", value: "U.K." },
            { key: "Statistic 2", value: "Unknown" },
            { key: "Statistic 3", value: "42" }   
         ]
      } 
   ], 
   onClick: function( clickedLabel ) {
      console.log( clickedLabel );
   }
});
```
